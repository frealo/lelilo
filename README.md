# LELILO

Latest EFI Linux Loader

Currently just ELILO with patches applied.

## Dependencies

gcc

binutils

gnu-efi

efibootmgr

## Compile

`make -j1`

## Install

_Must install manually for now._

`mkdir /usr/local/lib/elilo/`

`cp elilo.efi /usr/local/lib/elilo/`

`cp debian/elilo.sh /usr/local/sbin/elilo`

`cp tools/eliloalt /usr/local/sbin/`

`touch /etc/elilo.conf`

## Install Man Pages

`cp debian/elilo.8 /usr/local/share/man/man8/`

`cp debian/eliloalt.8 /usr/local/share/man/man8/`

# Post Install

## Config

Add your config to /etc/elilo.conf

### Sample Config 1

```
boot=/dev/sda1
install=/usr/lib/elilo/elilo.efi
delay=20
default=gentoo

image=/boot/kernel-2.6.12-gentoo-r6
	label=gentoo
	root=/dev/sda3
	read-only
```

### Sample Config 2


```
boot=/dev/nvme0n1p1
install=/usr/local/lib/elilo/elilo.efi
chooser=simple
prompt
delay=50
timeout=50
default=generic

#random config comment
image=/EFI/generic/boot/vmlinuz-6.4.10
 initrd=/EFI/generic/boot/initramfs-6.4.10.xz
 label=generic
 root=/dev/nvme0n1p5
 read-only
```

## Initrd / Initramfs & Kernel

The initrd and kernel must be in the same partition as the bootloader and elilo.conf autoplaced by elilo.

### Sample Layout

/boot/efi/EFI/generic/elilo.conf

/boot/efi/EFI/generic/elilo.efi

/boot/efi/EFI/generic/README.txt

/boot/efi/EFI/generic/boot/initramfs-6.4.10.xz

/boot/efi/EFI/generic/boot/vmlinuz-6.4.10

## Usage

elilo /dev/efi-partition

#### If sda1 is the efi partition:

`elilo /dev/sda1`

#### If nvme0n1p1 is the efi partition:

`elilo /dev/nvme0n1p1`
